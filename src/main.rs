// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#[rustfmt::skip]
mod config;

mod actions;
mod application;
mod migration;
mod utils;
mod widgets;

use gettextrs::{gettext, LocaleCategory};
use gtk::{gio, glib};

use self::application::Application;
use self::config::{GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE};

/// Application entry point.
fn main() -> glib::ExitCode {
    textpieces_core::init();

    // Initialize logger
    tracing_subscriber::fmt::init();

    // Prepare i18n
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    gettextrs::textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Set application name
    glib::set_application_name(&gettext("Text Pieces"));

    // Load resources from file
    let res = gio::Resource::load(RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&res);

    // Try to migrate from old Text Pieces
    if let Err(err) = migration::try_migrate() {
        tracing::warn!("Migration from old Text Pieces failed: {err}");
    }

    // Run the application itself
    Application::default().run()
}

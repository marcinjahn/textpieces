// SPDX-FileCopyrightText: 2023 Gleb Smirnov glebsmirnov0708@gmail.com
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::Path;

pub fn is_flatpak() -> bool {
    Path::new("/.flatpak-info").exists()
}

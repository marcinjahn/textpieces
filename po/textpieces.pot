# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

msgid ""
msgstr ""
"Project-Id-Version: textpieces\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-19 14:16+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Set application name
#: data/io.gitlab.liferooter.TextPieces.desktop.in.in:7
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:11 src/main.rs:33
#: src/widgets/window.rs:194
msgid "Text Pieces"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.desktop.in.in:8
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:12
#: src/widgets/window.rs:195
msgid "Developer's scratchpad"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/io.gitlab.liferooter.TextPieces.desktop.in.in:11
msgid ""
"GNOME;GTK;Text;Base64;JSON;YAML;Count;SHA1;SHA256;SHA384;SHA512;MD5;Checksum;"
"Escape;Encode;HTML;URL;Prettify;Filter;Reverse;Trim;"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.desktop.in.in:25
msgid "New Window"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:19
msgid "Color scheme"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:24
msgid "Font family"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:29
msgid "Font size"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:35
msgid "Window width"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:40
msgid "Window height"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:45
msgid "Window maximized state"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:51
msgid "The last used action ID"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:56
msgid "Parameter values of the last used action"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:62
msgid "Whether to wrap long lines"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:67
msgid "Whether to insert spaces instead of tabs"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:72
msgid "How many spaces are in a tab character"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:78
msgid "Whether a search match must start and end a word."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:83
msgid "Whether the search is case sensitive."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.gschema.xml.in:88
msgid "Whether to search by regular expressions."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:16
msgid ""
"Powerful scratchpad with ability to perform a lot of text transformations, "
"such as:"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:20
msgid "Calculate hashes"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:21
msgid "Encode text"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:22
msgid "Decode text"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:23
msgid "Remove trailing spaces and lines"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:24
msgid "Count lines, symbols and words"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:25
msgid "Format JSON and XML"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:26
msgid "Escape and unescape strings"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:27
msgid "Convert JSON to YAML and vice versa"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:28
msgid "Filter lines"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:29
msgid "And so on."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:31
msgid "The application is extendable with your own script-based actions."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:35
msgid "Transform text easily"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:39
msgid "Use a lot of pre-defined actions"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:43
msgid "Extend with your own actions"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:47
msgid "Actions are powerful with parameters"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:92
msgid "Bug fix: data migration from the old versions works as expected now"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:97
msgid "Changes:"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:99
msgid ""
"Redesigned interface. New Text Pieces is much more elegant and "
"straightforward."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:100
msgid ""
"Reworked architecture. The way how Text Pieces work internally was heavily "
"improved."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:101
msgid ""
"Typed parameters. Now action parameters can be boolean, integer or even real!"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:102
msgid ""
"Asynchronous actions. Application will never freeze because of running "
"action."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:103
msgid ""
"Action cancellation. Accidentally started an action with infinite loop "
"inside? Now you can cancel it."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:104
msgid ""
"More reliable built-in actions. Now built-in actions are written in Rust and "
"covered by tests, so they run faster and unlikely will be broken."
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:110
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:123
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:133
msgid "Bug fixes:"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:112
msgid "Script files can be correctly opened using an editor of your choice"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:113
msgid "Newly created script names are generated from tool name"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:119
msgid "New features:"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:121
msgid "Text Pieces now can open files"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:125
msgid "Some translation issues are fixed"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:126
msgid "Some tools are fixed and optimized"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:127
msgid "Shortcuts window is updated and fixed"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:135
msgid "Tool arguments now work as expected"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:136
msgid "Translate some untranslated strings"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:142
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:154
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:162
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:185
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:208
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:227
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:238
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:248
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:258
msgid "New features and improvements:"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:144
msgid "Add tool for extracting RSS URLs from OPML files"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:145
msgid "Use more verbose error reporting for Base64 decode tool"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:146
msgid "Focus editor on startup"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:147
msgid "Save selected tool between sessions"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:148
msgid "Minor UI improvements"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:156
msgid "Add hotkey to open search in replace mode"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:164
msgid "Add some keywords to the application for system search"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:170
msgid "New features and improvement:"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:172
msgid "Add style schemes"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:173
msgid "Add search and replace feature to editor"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:174
msgid "Fix some bugs"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:180
msgid "Minor bug fix"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:187
msgid "Refresh UI"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:188
msgid "Fix a lot of bugs"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:189
msgid "Add powerful settings"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:190
msgid "Add arguments support for custom tools"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:191
msgid "Add translation support"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:192
#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:231
msgid "Improve search"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:198
msgid "Fix theme switcher"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:203
msgid "Add theme switcher for system which don't support color schemes"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:210
msgid "Minor UI changes"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:211
msgid "Add XML formatter"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:217
msgid "Some bugfixes and improvements"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:222
msgid "Bring back tools with arguments"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:229
msgid "Redesign the application"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:230
msgid "Add custom tools support"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:232
msgid "Add ability to text load from file and save to file"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:240
msgid "Exit by Ctrl+Q"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:241
msgid "Monospace argument entry"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:242
msgid "Update new runtime"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:250
msgid "Sort tools"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:251
msgid "Filter tools"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:252
msgid "Minify C-like code"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:260
msgid "Add more tools"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:261
msgid "Make tools popover more powerful"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:262
msgid "Redesign icon"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:263
msgid "General improvements"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:269
msgid "Add tools for replacement and removing"
msgstr ""

#: data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in:274
msgid "Application release"
msgstr ""

#: resources/ui/action_page.blp:22
msgctxt "action settings"
msgid "_Create"
msgstr ""

#: resources/ui/action_page.blp:35
msgctxt "action settings"
msgid "D_elete"
msgstr ""

#: resources/ui/action_page.blp:45
msgctxt "action settings"
msgid "_Name"
msgstr ""

#: resources/ui/action_page.blp:50
msgctxt "action settings"
msgid "_Description"
msgstr ""

#: resources/ui/action_page.blp:61
msgctxt "action settings"
msgid "Action has no parameters"
msgstr ""

#: resources/ui/action_page.blp:62
msgctxt "action settings"
msgid ""
"Parameter values are asked before performing the action and passed to your "
"script as command line arguments."
msgstr ""

#: resources/ui/action_page.blp:71 resources/ui/action_page.blp:88
msgctxt "action settings"
msgid "_Add"
msgstr ""

#: resources/ui/action_page.blp:80
msgctxt "action settings"
msgid "Parameters"
msgstr ""

#: resources/ui/action_search.blp:45
msgctxt "action search"
msgid "No Actions Found"
msgstr ""

#: resources/ui/action_search.blp:57
msgctxt "action search"
msgid "_Create custom action"
msgstr ""

#: resources/ui/parameter_row.blp:22
msgctxt "parameter menu"
msgid "_Change Type"
msgstr ""

#: resources/ui/parameter_row.blp:23
msgctxt "parameter menu"
msgid "_Text"
msgstr ""

#: resources/ui/parameter_row.blp:24
msgctxt "parameter menu"
msgid "_Boolean"
msgstr ""

#: resources/ui/parameter_row.blp:25
msgctxt "parameter menu"
msgid "_Integer"
msgstr ""

#: resources/ui/parameter_row.blp:26
msgctxt "parameter menu"
msgid "_Real Number"
msgstr ""

#: resources/ui/parameter_row.blp:31
msgctxt "parameter menu"
msgid "_Delete"
msgstr ""

#: resources/ui/preferences.blp:10
msgctxt "preferences"
msgid "_Editor"
msgstr ""

#: resources/ui/preferences.blp:16
msgctxt "preferences"
msgid "Behavior"
msgstr ""

#: resources/ui/preferences.blp:19
msgctxt "preferences"
msgid "Wrap _Lines"
msgstr ""

#: resources/ui/preferences.blp:25
msgctxt "preferences"
msgid "_Tabs to Spaces"
msgstr ""

#: resources/ui/preferences.blp:31
msgctxt "preferences"
msgid "Tabs _Width in Spaces"
msgstr ""

#: resources/ui/preferences.blp:43
msgctxt "preferences"
msgid "Appearance"
msgstr ""

#: resources/ui/preferences.blp:46
msgctxt "preferences"
msgid "_Color Scheme"
msgstr ""

#: resources/ui/preferences.blp:51
msgctxt "preferences"
msgid "Follow System"
msgstr ""

#: resources/ui/preferences.blp:52
msgctxt "preferences"
msgid "Light"
msgstr ""

#: resources/ui/preferences.blp:53
msgctxt "preferences"
msgid "Dark"
msgstr ""

#: resources/ui/preferences.blp:59
msgctxt "preferences"
msgid "Font _Family"
msgstr ""

#: resources/ui/preferences.blp:77
msgctxt "preferences"
msgid "Font _Size"
msgstr ""

#: resources/ui/preferences.blp:90
msgctxt "preferences"
msgid "_Actions"
msgstr ""

#: resources/ui/preferences.blp:96
msgctxt "preferences"
msgid "Write your own actions"
msgstr ""

#: resources/ui/preferences.blp:110 resources/ui/preferences.blp:126
msgctxt "preferences"
msgid "_Create"
msgstr ""

#: resources/ui/preferences.blp:117
msgctxt "preferences"
msgid "Custom actions"
msgstr ""

#: resources/ui/search_bar.blp:48
msgctxt "search bar"
msgid "Previous Match"
msgstr ""

#: resources/ui/search_bar.blp:54
msgctxt "search bar"
msgid "Next Match"
msgstr ""

#: resources/ui/search_bar.blp:66
msgctxt "search bar"
msgid "Search & Replace"
msgstr ""

#: resources/ui/search_bar.blp:77
msgctxt "search bar"
msgid "Search Options"
msgstr ""

#: resources/ui/search_bar.blp:93
msgctxt "search bar"
msgid "Close Search"
msgstr ""

#: resources/ui/search_bar.blp:106
msgctxt "search bar"
msgid "Replace With"
msgstr ""

#: resources/ui/search_bar.blp:117
msgctxt "search bar"
msgid "Re_place"
msgstr ""

#: resources/ui/search_bar.blp:130
msgctxt "search bar"
msgid "Replace _All"
msgstr ""

#: resources/ui/search_bar.blp:147
msgctxt "search bar"
msgid "_Regular expressions"
msgstr ""

#: resources/ui/search_bar.blp:154
msgctxt "search bar"
msgid "_Case sensitive"
msgstr ""

#: resources/ui/search_bar.blp:161
msgctxt "search bar"
msgid "Match whole _word only"
msgstr ""

#: resources/ui/search_entry.blp:23
msgctxt "search entry"
msgid "Find"
msgstr ""

#: resources/ui/shortcuts.blp:15
msgctxt "shortcut window"
msgid "Editing"
msgstr ""

#: resources/ui/shortcuts.blp:18
msgctxt "shortcut window"
msgid "Apply Action"
msgstr ""

#: resources/ui/shortcuts.blp:23
msgctxt "shortcut window"
msgid "Undo"
msgstr ""

#: resources/ui/shortcuts.blp:28
msgctxt "shortcut window"
msgid "Redo"
msgstr ""

#: resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "Copy and Paste"
msgstr ""

#: resources/ui/shortcuts.blp:37
msgctxt "shortcut window"
msgid "Copy Selected"
msgstr ""

#: resources/ui/shortcuts.blp:42
msgctxt "shortcut window"
msgid "Copy All"
msgstr ""

#: resources/ui/shortcuts.blp:47
msgctxt "shortcut window"
msgid "Cut Selection"
msgstr ""

#: resources/ui/shortcuts.blp:52
msgctxt "shortcut window"
msgid "Paste"
msgstr ""

#: resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Working With Files"
msgstr ""

#: resources/ui/shortcuts.blp:61
msgctxt "shortcut window"
msgid "Load File"
msgstr ""

#: resources/ui/shortcuts.blp:66
msgctxt "shortcut window"
msgid "Save to File"
msgstr ""

#: resources/ui/shortcuts.blp:72
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: resources/ui/shortcuts.blp:75
msgctxt "shortcut window"
msgid "Quit"
msgstr ""

#: resources/ui/shortcuts.blp:80
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: resources/ui/shortcuts.blp:85
msgctxt "shortcut window"
msgid "Open Preferences"
msgstr ""

#: resources/ui/shortcuts.blp:91
msgctxt "shortcut"
msgid "Windows"
msgstr ""

#: resources/ui/shortcuts.blp:94
msgctxt "shortcut window"
msgid "Close Window"
msgstr ""

#: resources/ui/shortcuts.blp:99
msgctxt "shortcut window"
msgid "New Window"
msgstr ""

#: resources/ui/shortcuts.blp:105
msgctxt "shortcuts window"
msgid "Search"
msgstr ""

#: resources/ui/shortcuts.blp:108
msgctxt "shortcut window"
msgid "Find in Text"
msgstr ""

#: resources/ui/shortcuts.blp:113
msgctxt "shortcut window"
msgid "Find and Replace in Text"
msgstr ""

#: resources/ui/shortcuts.blp:118
msgctxt "shortcuts window"
msgid "Move to Next Match"
msgstr ""

#: resources/ui/shortcuts.blp:123
msgctxt "shortcuts window"
msgid "Move to Previous Match"
msgstr ""

#: resources/ui/window.blp:17
msgctxt "drag overlay"
msgid "Drop a file to load its content"
msgstr ""

#: resources/ui/window.blp:33
msgctxt "button tooltip"
msgid "Copy"
msgstr ""

#: resources/ui/window.blp:49
msgid "_Select action"
msgstr ""

#: resources/ui/window.blp:127
msgctxt "main menu"
msgid "_New Window"
msgstr ""

#: resources/ui/window.blp:131
msgctxt "main menu"
msgid "_Save As…"
msgstr ""

#: resources/ui/window.blp:132
msgctxt "main menu"
msgid "_Load File…"
msgstr ""

#: resources/ui/window.blp:136
msgctxt "main menu"
msgid "_Preferences"
msgstr ""

#: resources/ui/window.blp:137
msgctxt "main menu"
msgid "_Keyboard Shortcuts"
msgstr ""

#: resources/ui/window.blp:138
msgctxt "main menu"
msgid "_About Text Pieces"
msgstr ""

#: src/utils/file_filter.rs:17
msgid "Text Files"
msgstr ""

#: src/utils/file_filter.rs:21
msgid "All Files"
msgstr ""

#. Set correct page title
#: src/widgets/action_page.rs:285 src/widgets/action_page.rs:306
msgctxt "action settings"
msgid "Edit Action"
msgstr ""

#: src/widgets/parameter_row.rs:98
msgctxt "parameter row"
msgid "Text"
msgstr ""

#: src/widgets/parameter_row.rs:99
msgctxt "parameter row"
msgid "Boolean"
msgstr ""

#: src/widgets/parameter_row.rs:100
msgctxt "parameter row"
msgid "Integer"
msgstr ""

#: src/widgets/parameter_row.rs:101
msgctxt "parameter row"
msgid "Real Number"
msgstr ""

#: src/widgets/preferences.rs:199
msgctxt "preferences"
msgid "Edit _Script"
msgstr ""

#: src/widgets/preferences.rs:203
msgctxt "preferences"
msgid "Edit _Metadata"
msgstr ""

#: src/widgets/preferences.rs:207
msgctxt "preferences"
msgid "_Delete"
msgstr ""

#: src/widgets/preferences.rs:214
msgctxt "preferences"
msgid "Options"
msgstr ""

#: src/widgets/preferences.rs:360
msgctxt "preferences"
msgid "Failed to create an action: "
msgstr ""

#. Credits
#: src/widgets/window.rs:203
msgid "Gleb Smirnov"
msgstr ""

#: src/widgets/window.rs:204
msgid "Gleb Smirnov <glebsmirnov0708@gmail.com>"
msgstr ""

#: src/widgets/window.rs:205
msgid "Tobias Bernard https://tobiasbernard.com"
msgstr ""

#: src/widgets/window.rs:206
msgid "translator-credits"
msgstr ""

#: src/widgets/window.rs:219
msgid "Text is copied"
msgstr ""

#: src/widgets/window.rs:293 src/widgets/window.rs:309
msgid "Failed to get file path"
msgstr ""

#: src/widgets/window.rs:398
msgid "Set action parameters"
msgstr ""

#: src/widgets/window.rs:403
msgid "_Apply"
msgstr ""

#: src/widgets/window.rs:404
msgid "_Cancel"
msgstr ""

#: src/widgets/window.rs:578
msgid "Action is running for too long. Dismiss to cancel it."
msgstr ""

#: src/widgets/window.rs:595
msgid "Action was cancelled"
msgstr ""

#: src/widgets/window.rs:660
msgid "Load File"
msgstr ""

#: src/widgets/window.rs:681
msgid "Save File"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Allow Escape Sequences"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Base64 Decode"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Base64 Encode"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Compute MD5 hash sum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Compute SHA1 hash sum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Compute SHA256 hash sum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Compute SHA384 hash sum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Compute SHA512 hash sum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Convert JSON to YAML"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Convert OPML to RSS link"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Convert YAML to JSON"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Count Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Count Symbols"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Count World"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Count how many symbols are in text"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Count how many words are in text"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Decode text from Base64"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Deduplicate"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Deduplicate Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Encode text to Base64"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Escape HTML"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Escape String"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Escape URL"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Extract RSS links from OPML outline"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Filter Lines by Regex"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Filter Lines by Substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Leave only duplicate lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "MD5 Checksum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Minify JSON"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Parse JSON and convert it to YAML document"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Parse JSON and minify it"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Parse JSON and prettify it"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Parse XML and prettify it"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Parse YAML and convert it to JSON"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Prettify JSON"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Prettify XML"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Regex"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove All"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove Non-Consecutive"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove Regex"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove Substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove Trailing"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove Unique Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove all lines that contain matches to the regular expression"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove all lines that do not contain matches to the regular expression"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove all lines which contain the substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove all lines which don't contain the substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove all matches of the regular expression from the text"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove all occurences of the substring from the text"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove duplicate lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove leading and trailing whitespaces and newlines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Remove trailing whitespaces and newlines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reorder characters in reverse order"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reorder lines in reverse order"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Repex"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace HTML sequences with characters"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace Regex"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace Substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace URL sequences with characters"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace With"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace all matches of the regular expression"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace all occurences of the substring with in the text with another string"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace escape sequences with characters"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace special characters with HTML-safe sequences"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace special characters with URL-safe sequences"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Replace special characters with escape sequences"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reverse Filter Lines by Regex"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reverse Filter Lines by Substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reverse Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reverse Sort Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Reverse Text"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "SHA1 Checksum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "SHA256 Checksum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "SHA384 Checksum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "SHA512 Checksum"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Sort Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Sort lines in text in alphabetical order"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Sort lines in text in reverse alphabetical order"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Substring"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Trim Lines"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Unescape HTML"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Unescape String"
msgstr ""

#. Translators: generated from built-in actions
msgctxt "action"
msgid "Unescape URL"
msgstr ""

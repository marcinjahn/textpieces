<?xml version="1.0" encoding="UTF-8"?>

<!--
SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

<component type="desktop-application">
  <id>@app-id@</id>
  <name>Text Pieces</name>
  <summary>Developer's scratchpad</summary>
  <metadata_license>CC0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <description>
    <p>
      Powerful scratchpad with ability to perform a lot of text transformations, such as:
    </p>
    <ul>
      <li>Calculate hashes</li>
      <li>Encode text</li>
      <li>Decode text</li>
      <li>Remove trailing spaces and lines</li>
      <li>Count lines, symbols and words</li>
      <li>Format JSON and XML</li>
      <li>Escape and unescape strings</li>
      <li>Convert JSON to YAML and vice versa</li>
      <li>Filter lines</li>
      <li>And so on.</li>
    </ul>
    <p>The application is extendable with your own script-based actions.</p>
  </description>
  <screenshots>
    <screenshot type="default">
      <caption>Transform text easily</caption>
      <image type="source">https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-1.png</image>
    </screenshot>
    <screenshot>
      <caption>Use a lot of pre-defined actions</caption>
      <image type="source">https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-2.png</image>
    </screenshot>
    <screenshot>
      <caption>Extend with your own actions</caption>
      <image type="source">https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-3.png</image>
    </screenshot>
    <screenshot>
      <caption>Actions are powerful with parameters</caption>
      <image type="source">https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-4.png</image>
    </screenshot>
  </screenshots>
  <branding>
    <color type="primary" scheme_preference="light">#fa968a</color>
    <color type="primary" scheme_preference="dark">#7d121a</color>
  </branding>

  <url type="homepage">https://gitlab.com/liferooter/textpieces</url>
  <url type="bugtracker">https://gitlab.com/liferooter/textpieces/-/issues</url>
  <developer id="io.gitlab.liferooter">
    <name>Gleb Smirnov</name>
  </developer>
  <update_contact>glebsmirnov0708@gmail.com</update_contact>

  <content_rating type="oars-1.0" />
  <supports>
    <control>pointing</control>
    <control>touch</control>
  </supports>
  <recommends>
    <control>keyboard</control>
  </recommends>
  <requires>
    <display_length compare="ge">360</display_length>
    <internet>offline-only</internet>
  </requires>
  <kudos>
     <kudo>ModernToolkit</kudo>
     <kudo>HiDpiIcon</kudo>
  </kudos>

  <translation type="gettext">@gettext-package@</translation>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <provides>
    <binary>textpieces</binary>
  </provides>

  <replaces>
    <id>com.github.liferooter.textpieces</id>
  </replaces>
  <releases>
    <release version="4.0.6" date="2024-05-18">
      <description>
        <p>Bug fix: data migration from the old versions works as expected now</p>
      </description>
    </release>
    <release version="4.0.0" date="2024-04-30">
      <description>
        <p>Changes:</p>
        <ul>
          <li>Redesigned interface. New Text Pieces is much more elegant and straightforward.</li>
          <li>Reworked architecture. The way how Text Pieces work internally was heavily improved.</li>
          <li>Typed parameters. Now action parameters can be boolean, integer or even real!</li>
          <li>Asynchronous actions. Application will never freeze because of running action.</li>
          <li>Action cancellation. Accidentally started an action with infinite loop inside? Now you can cancel it.</li>
          <li>More reliable built-in actions. Now built-in actions are written in Rust and covered by tests, so they run faster and unlikely will be broken.</li>
        </ul>
      </description>
    </release>
    <release version="3.4.1" date="2022-12-21">
      <description>
        <p>Bug fixes:</p>
        <ul>
          <li>Script files can be correctly opened using an editor of your choice</li>
          <li>Newly created script names are generated from tool name</li>
        </ul>
      </description>
    </release>
    <release version="3.4.0" date="2022-12-17">
      <description>
        <p>New features:</p>
        <ul>
          <li>Text Pieces now can open files</li>
        </ul>
        <p>Bug fixes:</p>
        <ul>
          <li>Some translation issues are fixed</li>
          <li>Some tools are fixed and optimized</li>
          <li>Shortcuts window is updated and fixed</li>
        </ul>
      </description>
    </release>
    <release version="3.3.1" date="2022-11-01">
      <description>
        <p>Bug fixes:</p>
        <ul>
          <li>Tool arguments now work as expected</li>
          <li>Translate some untranslated strings</li>
        </ul>
      </description>
    </release>
    <release version="3.3.0" date="2022-10-24">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Add tool for extracting RSS URLs from OPML files</li>
          <li>Use more verbose error reporting for Base64 decode tool</li>
          <li>Focus editor on startup</li>
          <li>Save selected tool between sessions</li>
          <li>Minor UI improvements</li>
        </ul>
      </description>
    </release>
    <release version="3.2.0" date="2022-09-21">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Add hotkey to open search in replace mode</li>
        </ul>
      </description>
    </release>
    <release version="3.1.1" date="2022-09-21">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Add some keywords to the application for system search</li>
        </ul>
      </description>
    </release>
    <release version="3.1.0" date="2022-08-04">
      <description>
        <p>New features and improvement:</p>
        <ul>
          <li>Add style schemes</li>
          <li>Add search and replace feature to editor</li>
          <li>Fix some bugs</li>
        </ul>
      </description>
    </release>
    <release version="3.0.2" date="2021-12-16">
      <description>
        <p>Minor bug fix</p>
      </description>
    </release>
    <release version="3.0.1" date="2021-12-16">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Refresh UI</li>
          <li>Fix a lot of bugs</li>
          <li>Add powerful settings</li>
          <li>Add arguments support for custom tools</li>
          <li>Add translation support</li>
          <li>Improve search</li>
        </ul>
      </description>
    </release>
    <release version="2.3.2" date="2021-09-30">
      <description>
        <p>Fix theme switcher</p>
      </description>
    </release>
    <release version="2.3.1" date="2021-09-30">
      <description>
        <p>Add theme switcher for system which don't support color schemes</p>
      </description>
    </release>
    <release version="2.2.1" date="2021-09-29">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Minor UI changes</li>
          <li>Add XML formatter</li>
        </ul>
      </description>
    </release>
    <release version="2.1.2" date="2021-09-23">
      <description>
        <p>Some bugfixes and improvements</p>
      </description>
    </release>
    <release version="2.1.1" date="2021-09-08">
      <description>
        <p>Bring back tools with arguments</p>
      </description>
    </release>
    <release version="2.0.1" date="2021-08-18">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Redesign the application</li>
          <li>Add custom tools support</li>
          <li>Improve search</li>
          <li>Add ability to text load from file and save to file</li>
        </ul>
      </description>
    </release>
    <release version="1.3.0" date="2021-04-25">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Exit by Ctrl+Q</li>
          <li>Monospace argument entry</li>
          <li>Update new runtime</li>
        </ul>
      </description>
    </release>
    <release version="1.2.0" date="2021-04-03">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Sort tools</li>
          <li>Filter tools</li>
          <li>Minify C-like code</li>
        </ul>
      </description>
    </release>
    <release version="1.1.0" date="2021-04-01">
      <description>
        <p>New features and improvements:</p>
        <ul>
          <li>Add more tools</li>
          <li>Make tools popover more powerful</li>
          <li>Redesign icon</li>
          <li>General improvements</li>
        </ul>
      </description>
    </release>
    <release version="1.0.2" date="2021-03-13">
      <description>
        <p>Add tools for replacement and removing</p>
      </description>
    </release>
    <release version="1.0.0" date="2021-03-10">
      <description>
        <p>Application release</p>
      </description>
    </release>
  </releases>
</component>

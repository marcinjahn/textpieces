# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

{
  stdenv,
  lib,
  rustPlatform,
  wrapGAppsHook4,
  meson,
  rustc,
  glib,
  pkg-config,
  gtk4,
  libadwaita,
  gtksourceview5,
  cargo, blueprint-compiler, desktop-file-utils, ninja,
}:
stdenv.mkDerivation (self: {
  pname = "textpieces";
  version = lib.pipe ./Cargo.toml [
    builtins.readFile
    builtins.fromTOML
    (cargoToml: cargoToml.package.version)
  ];

  src = ./.;

  cargoDeps = rustPlatform.importCargoLock {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "textpieces-core-1.0.0" = "sha256-HaLkL2HhH1khwsSdH64pZYtJ/WG+MLiEQPScDte/PAg=";
    };
  };

  nativeBuildInputs = [
    wrapGAppsHook4
    rustPlatform.cargoSetupHook
    cargo
    rustc
    meson
    ninja
    pkg-config
    blueprint-compiler
    desktop-file-utils
  ];

  buildInputs = [
    glib
    gtk4
    libadwaita
    gtksourceview5
  ];

  meta = with lib; {
    homepage = "https://gitlab.com/liferooter/textpieces";
    description = "Developer's scratchpad";
    license = licenses.gpl3Plus;
    platforms = platforms.linux;
  };
})
